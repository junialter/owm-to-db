# owm-to-db
This is a simple script that fetches OpenWeatherMap data from the API and stores it into a database.
Currently it uses postgresql, it could be adjusted for other DB backends.

You can run in on a regular basis in order to collect the data over time.
If you do that, take care to not hit the API threshold.
Set timethreshold to a higher value if in doubt. timethreshold set to 300 means the request will hit every 5 minutes (300 seconds)

- Enter your api key
- Enter your longitude latitude

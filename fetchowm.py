import requests
import json
import time

# Change to your needs
api_key = "0123456789abcdef0123456789abcdef"
lat = "48.208176"
lon = "16.373819"
timethreshold = 300
# Do not change
url = "https://api.openweathermap.org/data/2.5/onecall?lat=%s&lon=%s&appid=%s&units=metric" % (lat, lon, api_key)
intervaltime = time.time()
notyetrequested = True

while True:
    currentintervaltime = time.time()
    if (currentintervaltime - intervaltime) > timethreshold or notyetrequested:
        intervaltime = time.time()
        response = requests.get(url)
        if response.ok:
            notyetrequested = False
            #print(response.text)
            data = json.loads(response.text)
        else:
            notyetrequested = False
            print("Error fetching data from api.")

